angular
    .module('prototype', ['ng.jsoneditor','ngJsonExplorer'])
    .controller('PrototypeCtrl', ['$scope', '$http', function ($scope, $http) {
        var json = {"Array": [1, 2, 3], "Boolean": true, "Null": null, "Number": 123, "Object": {"a": "b", "c": "d"}, "String": "Hello World"};


        $scope.obj = {data: json, options: {mode: 'code'}};
        $scope.uuid = "";
        $scope.url = "";

        $scope.edit = function () {
           $scope.obj.options.mode = 'code';
        };

        $scope.save = function () {
           $scope.obj.options.mode = 'tree';

           $http.post("http://localhost:8080/prototype", $scope.obj.data, {})
               .success(function (data, status, headers, config)
               {
                 $scope.uuid = data.uuid;
                 $scope.url = "http://localhost:8080/prototype?uuid=" + $scope.uuid;
               })
               .error(function (data, status, headers, config)
               {
                    window.alert("Save error. Status := " + status);
               });
        };
    }]);