package com.zyan.limited.persistence;

import com.googlecode.objectify.ObjectifyService;
import com.zyan.limited.model.PrototypeEntity;
import com.zyan.limited.persistence.translators.JSONStringTranslatorFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class OfyHelper implements ServletContextListener {
    public static void register() {
        ObjectifyService.factory().getTranslators().add(new JSONStringTranslatorFactory());
        ObjectifyService.register(PrototypeEntity.class);
    }

    public void contextInitialized(ServletContextEvent event) {
        ServletContext sc = event.getServletContext();
        sc.log("OfyHelper: Init");

        // This will be invoked as part of a warmup request, or the first user
        // request if no warmup request was invoked.
        register();
    }

    public void contextDestroyed(ServletContextEvent event) {
        // App Engine does not currently invoke this method.
    }
}