package com.zyan.limited.persistence.translators;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.googlecode.objectify.impl.Path;
import com.googlecode.objectify.impl.translate.*;

public class JSONStringTranslatorFactory extends ValueTranslatorFactory<JsonObject, String> {
    public JSONStringTranslatorFactory() {
        super(JsonObject.class);
    }

    protected ValueTranslator<JsonObject, String> createValueTranslator(TypeKey<JsonObject> tk, CreateContext ctx, Path path) {
        return new ValueTranslator<JsonObject, String>(String.class) {
            protected JsonObject loadValue(String value, LoadContext ctx, Path path) throws SkipException {
                if (Strings.isNullOrEmpty(value)) return null;

                return new JsonParser().parse(value).getAsJsonObject();
            }

            protected String saveValue(JsonObject value, boolean index, SaveContext ctx, Path path) throws SkipException {
                if (null == value) return null;

                return new Gson().toJson(value);
            }
        };
    }
}