package com.zyan.limited.model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.UUID;

@Entity
public class PrototypeEntity {
    @Id
    private String id;

    private JsonObject json;

    public PrototypeEntity() {}

    private PrototypeEntity(Builder builder) {
        setId(builder.id);
        setJson(builder.json);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JsonObject getJson() {
        return json;
    }

    public void setJson(JsonObject json) {
        this.json = json;
    }

    public static final class Builder {
        private String id;
        private JsonObject json;

        public Builder() {
        }

        public Builder id(String val) {
            id = val;
            return this;
        }

        public Builder id(UUID val) {
            id = val.toString();
            return this;
        }

        public Builder json(JsonObject val) {
            json = val;
            return this;
        }

        public Builder json(String val) {
            json = new JsonParser().parse(val).getAsJsonObject();
            return this;
        }

        public PrototypeEntity build() {
            return new PrototypeEntity(this);
        }
    }
}
