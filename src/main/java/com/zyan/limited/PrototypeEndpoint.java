package com.zyan.limited;

import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.googlecode.objectify.Key;
import com.zyan.limited.model.PrototypeEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class PrototypeEndpoint extends HttpServlet {

    private static final Logger logger = Logger.getLogger(PrototypeEndpoint.class.getSimpleName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String json = CharStreams.toString(request.getReader());

        if(Strings.isNullOrEmpty(json)) {
            logger.warning("There is no json body in POST request. Bad request.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        Key<PrototypeEntity> key = ofy().save().entity(new PrototypeEntity.Builder()
                .id(UUID.randomUUID())
                .json(json)
                .build())
                .now();

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(String.format("{\"uuid\" : \"%s\"}", key.getName()));
        out.flush();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String uuid = request.getParameter("uuid");

        if(Strings.isNullOrEmpty(uuid)) {
            logger.warning("There is no uuid parameter. Bad request.");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        PrototypeEntity entity = ofy().load().key(Key.create(PrototypeEntity.class, uuid)).safe();

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(new Gson().toJson(entity));
        out.flush();
    }
}
