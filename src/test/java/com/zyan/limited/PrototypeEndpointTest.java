package com.zyan.limited;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;
import com.zyan.limited.persistence.OfyHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link PrototypeEndpoint}.
 */

@RunWith(JUnit4.class)
public class PrototypeEndpointTest {
    private static final String FAKE_URL = "fake.fk/hello";

    // Set up a helper so that the ApiProxy returns a valid environment for local testing.
    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    private Closeable closeable;

    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;
    private StringWriter responseWriter;
    private PrototypeEndpoint servletUnderTest;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        helper.setUp();

        closeable = ObjectifyService.begin();
        OfyHelper.register();

        //  Set up some fake HTTP requests
        when(mockRequest.getRequestURI()).thenReturn(FAKE_URL);

        // Set up a fake HTTP response.
        responseWriter = new StringWriter();
        when(mockResponse.getWriter()).thenReturn(new PrintWriter(responseWriter));

        servletUnderTest = new PrototypeEndpoint();
    }

    @After
    public void tearDown() {
        closeable.close();
        helper.tearDown();
    }

    private String doPost() throws Exception {
        when(mockRequest.getReader()).thenReturn(new BufferedReader(new StringReader("{\"property\" : \"a\"}")));

        servletUnderTest.doPost(mockRequest, mockResponse);

        return responseWriter.toString();
    }

    @Test
    public void doPost_successful() throws Exception {
        String json = doPost();

        assertThat(json)
                .contains("uuid");
        assertThat(json)
                .doesNotContain("property");
    }

    @Test
    public void doGet_afterPost() throws Exception {

        JsonObject json = new JsonParser().parse(doPost()).getAsJsonObject();

        when(mockRequest.getParameter("uuid")).thenReturn(json.get("uuid").getAsString());

        assertThat(responseWriter.toString())
                .isNotEmpty();
    }

    @Test
    public void doGet_assert_no_entity() throws Exception {
        servletUnderTest.doGet(mockRequest, mockResponse);
        // We expect to get a bad request with no response body
        assertThat(responseWriter.toString())
                .isEmpty();
    }
}
